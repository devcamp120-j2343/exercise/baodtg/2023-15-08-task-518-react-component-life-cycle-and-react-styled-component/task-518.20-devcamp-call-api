import { Component } from "react";
import axios from "axios";

class AxiosLibrary extends Component {

    axiousLibraryCallAPI = async (config) => {
        let response = await axios(config);
        return response.data
    }

    getByIdHandler = () => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/1',
            headers: {}
        };

        this.axiousLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })


    }

    getAllHandler = () => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/',
            headers: {}
        };

        this.axiousLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }

    postHandler = () => {
        let data = JSON.stringify({
            "userId": 10,
            "title": "sunt aut ",
            "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
        });

        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        this.axiousLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })


    }

    updateHandler = () => {
        let data = JSON.stringify({
            "userId": 10,
            "title": "sunt  ",
            "body": "quia "
        });

        let config = {
            method: 'put',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/2',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        this.axiousLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }

    deleteHanlder = () => {
        let config = {
            method: 'delete',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/2',
            headers: {}
        };

        this.axiousLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }


    render() {
        return (
            <div className="row">
                <div className="col">
                    <button className="btn btn-primary" onClick={this.getByIdHandler}>Get by ID</button>
                </div>
                <div className="col">
                    <button className="btn btn-primary" onClick={this.getAllHandler}>Get All</button>
                </div>
                <div className="col">
                    <button className="btn btn-primary" onClick={this.postHandler}> Post Create</button>
                </div>
                <div className="col">
                    <button className="btn btn-primary" onClick={this.updateHandler}>Put Update</button>
                </div>
                <div className="col">
                    <button className="btn btn-primary" onClick={this.deleteHanlder}>Delete</button>
                </div>
            </div>
        )
    }
}
export default AxiosLibrary