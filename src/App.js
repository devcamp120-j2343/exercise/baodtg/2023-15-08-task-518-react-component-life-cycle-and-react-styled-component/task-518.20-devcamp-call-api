import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import FetchAPI from './components/FetchAPI';
import AxiosLibrary from './components/AxiosLibrary';

function App() {
  return (
    <div className='container mt-5'>
      <FetchAPI />
      <hr/>
      <AxiosLibrary />
    </div>
  );
}

export default App;
