import { Component } from "react";

class FetchAPI extends Component {

    fetchAPI = async (url, requestOptions) => {
        let response = await fetch(url, requestOptions);
        let data = await response.json();
        return data
    }

    getByIdHandler = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        var url = "https://jsonplaceholder.typicode.com/posts/1"

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })


    }

    getAllHandler = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        }
        var url = "https://jsonplaceholder.typicode.com/posts/"
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }

    postHandler = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "userId": 15,
            "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
            "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        var url = "https://jsonplaceholder.typicode.com/posts/"
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })


    }

    updateHandler = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "userId": 3,
            "title": "sunt aut ",
            "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
        });

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        var url = "https://jsonplaceholder.typicode.com/posts/1"
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }

    deleteHanlder = () => {
        var requestOptions = {
            method: 'DELETE',
            redirect: 'follow'
        };
        var url = "https://jsonplaceholder.typicode.com/posts/1"
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }


    render() {
        return (
            <div className="row">
                <div className="col">
                    <button className="btn btn-primary" onClick={this.getByIdHandler}>Get by ID</button>
                </div>
                <div className="col">
                    <button className="btn btn-primary" onClick={this.getAllHandler}>Get All</button>
                </div>
                <div className="col">
                    <button className="btn btn-primary" onClick={this.postHandler}> Post Create</button>
                </div>
                <div className="col">
                    <button className="btn btn-primary" onClick={this.updateHandler}>Put Update</button>
                </div>
                <div className="col">
                    <button className="btn btn-primary" onClick={this.deleteHanlder}>Delete</button>
                </div>
            </div>
        )
    }
}
export default FetchAPI